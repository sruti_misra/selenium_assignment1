package Assignment1;

import java.util.concurrent.TimeUnit;

import org.openqa.selenium.By;
import org.openqa.selenium.WebDriver;
import org.openqa.selenium.chrome.ChromeDriver;
import org.openqa.selenium.firefox.FirefoxDriver;
import org.testng.annotations.AfterTest;
import org.testng.annotations.BeforeTest;
import org.testng.annotations.Parameters;
import org.testng.annotations.Test;

public class NewTest {
	public WebDriver driver;

	@BeforeTest
	@Parameters("browser")
	public void openBrowser(String browser) {

		if (browser.equalsIgnoreCase("firefox")) {

			System.setProperty("webdriver.gecko.driver", "WebdriverStorage/geckodriver.exe");
			driver = new FirefoxDriver();
			
			
		}

		if (browser.equalsIgnoreCase("chrome")) {
			System.setProperty("webdriver.chrome.driver", "WebdriverStorage/chromedriver.exe");
			driver = new ChromeDriver();
		}

		driver.manage().deleteAllCookies();
		driver.manage().window().maximize();
		driver.manage().timeouts().implicitlyWait(50, TimeUnit.SECONDS);
		driver.manage().timeouts().pageLoadTimeout(50, TimeUnit.SECONDS);
		driver.get("https://www.tripadvisor.in/");

	}

	@Test
	public void tripAdvisor() {
		
			try {
				driver.findElement(By.xpath(".//*[@id='taplc_global_nav_action_profile_0']/div/a[2]")).click();
				Thread.sleep(1000);
				driver.switchTo().frame("overlayRegFrame");
				Thread.sleep(1000);
				driver.findElement(By.id("googleBtn")).click();
				//String winHandleBefore = driver.getWindowHandle();
				for (String winHandle : driver.getWindowHandles()) {
					driver.switchTo().window(winHandle);
				}
				driver.findElement(By.id("identifierId")).sendKeys("kumartullu567@gmail.com");
				driver.findElement(By.xpath(".//*[@id='identifierNext']/content/span")).click();
				driver.findElement(By.name("password")).sendKeys("9335905467");
				
			} catch (InterruptedException e) {
				
				e.printStackTrace();
			}
			
		}
	
	@AfterTest
	public void afterTest() {
		driver.quit();
	}
}